package main;

import java.io.IOException;
import java.io.InputStream;

import javax.swing.JFrame;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

public class Main {

    public static void main(String[] args) throws IOException, InterruptedException {
    	
    	JFrame frame = new JFrame ("MyPanel");
    	frame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
    	frame.getContentPane().add (new MyPanel());
    	frame.pack();
    	frame.setVisible (true);
		frame.setLocationRelativeTo(null);
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		frame.setResizable(true);

		ReadProperties properties = new ReadProperties();
		    	
    	String host = properties.get("ip");
    	int port = Integer.valueOf(properties.get("port"));
	    String user = properties.get("user");;
	    String password = properties.get("password");;
	    String file = args[0];
	    String command = file + " " + properties.get("config_file");
	    
	    try{
	    	
	    	java.util.Properties config = new java.util.Properties(); 
	    	config.put("StrictHostKeyChecking", "no");
	    	JSch jsch = new JSch();
	    	Session session=jsch.getSession(user, host, port);
	    	session.setPassword(password);
	    	session.setConfig(config);
	    	session.connect();
	    	System.out.println("Connected to " + host + ":" + port + ".");
	    	System.out.println("Running command " + command + "...");
	    	
	    	Channel channel=session.openChannel("exec");
	        ((ChannelExec)channel).setCommand(command);
	        channel.setInputStream(null);
	        ((ChannelExec)channel).setErrStream(System.err);
	        
	        InputStream in=channel.getInputStream();
	        channel.connect();
	        byte[] tmp=new byte[1024];
	        while(true){
	        	while(in.available()>0){
	        		int i=in.read(tmp, 0, 1024);
	        		if(i<0)break;
	        		System.out.print(new String(tmp, 0, i));
	        	}
	        	if(channel.isClosed()){
	        		System.out.println("exit-status: " + channel.getExitStatus());
	        		break;
	        	}
	        	try{
	        		Thread.sleep(1000);
	        	}catch(Exception ee){}
	        }
	        channel.disconnect();
	        session.disconnect();
	        System.out.println("DONE");
	    }catch(Exception e){
	    	e.printStackTrace();
	    }
    }
}
