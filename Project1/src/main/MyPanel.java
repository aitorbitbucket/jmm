package main;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

// NEW 
// import java.awt.*;
// import java.awt.event.*;
// import javax.swing.*;
// import javax.swing.event.*;
// NEW
public class MyPanel extends JPanel {
   /**
    *
    */
   private static final long serialVersionUID = 1L;
   private JTextField jm_text1;
   private JLabel jm_label1;
   private JLabel jm_label2;
   private JButton jm_button1;

   public MyPanel() {
      //construct components
      jm_text1 = new JTextField (1);
      // Texto 
      jm_label1 = new JLabel ("Label1");
      // Texto 
      jm_label2 = new JLabel ("Label2");
      // Button
      jm_button1 = new JButton ("Button1");

      //adjust size and set layout
      setPreferredSize (new Dimension (500, 500));
      setLayout (null);

      //add components
      add (jm_text1);
      //add (jm_label1);
      //add (jm_label2);
      add (jm_button1);

      //set component bounds (only needed by Absolute Positioning)
      jm_text1.setBounds (245, 50, 60, 25);
      jm_label1.setBounds (35, 30, 185, 50);
      jm_label2.setBounds (250, 30, 60, 20);
      jm_button1.setBounds (0, 0, 315, 25);

         jm_button1.addActionListener( new ActionListener()
      {
         public void actionPerformed(ActionEvent e)
         {

         }
      });
   }
}