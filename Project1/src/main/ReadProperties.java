package main;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class ReadProperties {
	
	private Properties properties = new Properties();
	
	public ReadProperties()
    {
        try
        {
            InputStream inputStream = new FileInputStream("config.properties");
            properties.load(inputStream);
            inputStream.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    
    public String get(String key)
    {
        return this.properties.getProperty(key);
    }

}