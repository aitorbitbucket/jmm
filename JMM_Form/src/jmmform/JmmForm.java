package jmmform;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Hashtable;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;

public class JmmForm extends JFrame implements ActionListener {

    private static final long serialVersionUID = 7217085894535888081L;

    private String l_pc_id;
    private JLabel l_host_ip;
    private JTextField tf_mcas_ip;

    private JLabel l_host_user;
    private JTextField tf_mcas_user;

    private JLabel l_host_pass;
    private JPasswordField tf_mcas_pass;

    private JTextField tf_Status;
    private JLabel l_connect;
    
    private JButton bConectar;

    // Manage Debuglog logic
    private JTextArea t_debug;
    private JScrollPane sCrollDebug;

    private JButton bDebuglog;
    private JButton bSaveDebug;
    private Session SDebuglog;
    private ChannelExec CDebuglog;
    private JTextField tf_DeStatus;

    // Manage Debuglog logic
    private JTextArea t_omlog;
    private JScrollPane sCrollOmlog;
    private JButton bOMlog;
    private JButton bSaveOM;
    private Session SOMlog;
    private ChannelExec COMlog;
    private JTextField tf_OMStatus;

    // Manage tabs
    private JTabbedPane tb_panels;

    public JmmForm() {

        // setLayout(null);
        GraphicsEnvironment environment = GraphicsEnvironment.getLocalGraphicsEnvironment();
        Rectangle bounds = environment.getMaximumWindowBounds();
        System.out.println("Screen Bounds = " + bounds.getWidth());
        System.out.println("Screen Bounds = " + bounds.getHeight());

        // Parametros asociados a la ventana
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setExtendedState(MAXIMIZED_BOTH);

        // Creamos el conjunto de tb_panels
        tb_panels = new JTabbedPane();
        // Creamos el panel y lo añadimos a las tb_panels
        JPanel panel1 = new JPanel();
        panel1.setLayout(null);
        panel1.setVisible(true);
        panel1.setBounds(0, 0, 1000, 1000);
        //Componentes del panel1
        l_pc_id = "";
        int Xposs = 20;
        int Yposs = 20;
        l_host_ip = new JLabel("MCAS IP:");
        l_host_ip.setBounds(Xposs, Yposs, 60, 25);
        panel1.add(l_host_ip);

        Xposs = Xposs + 65;
        tf_mcas_ip = new JTextField("");
        tf_mcas_ip.setBounds(Xposs, Yposs, 100, 25);
        panel1.add(tf_mcas_ip);

        Xposs = Xposs + 105;
        l_host_user = new JLabel("USER:");
        l_host_user.setBounds(Xposs, Yposs, 40, 25);
        panel1.add(l_host_user);

        Xposs = Xposs + 45;
        tf_mcas_user = new JTextField("");
        tf_mcas_user.setBounds(Xposs, Yposs, 40, 25);
        panel1.add(tf_mcas_user);

        Xposs = Xposs + 45;
        l_host_pass = new JLabel("PASSWORD:");
        l_host_pass.setBounds(Xposs, Yposs, 75, 25);
        panel1.add(l_host_pass);

        Xposs = Xposs + 80;
        tf_mcas_pass = new JPasswordField(20);
        tf_mcas_pass.setBounds(Xposs, Yposs, 50, 25);
        panel1.add(tf_mcas_pass);

        Xposs = Xposs + 55;
        bConectar = new JButton("Connect");
        bConectar.setBounds(Xposs, Yposs, 100, 25);
        panel1.add(bConectar);
        bConectar.addActionListener(this);

        Xposs = Xposs + 105;
        tf_Status = new JTextField("");
        tf_Status.setBounds(Xposs, Yposs, 20, 20);
        tf_Status.setBackground(Color.gray);
        tf_Status.setEnabled(false);
        tf_Status.setOpaque(false);
        LineBorder lineBorder = new LineBorder(Color.gray, 20, true);
        tf_Status.setBorder(lineBorder);
        panel1.add(tf_Status);

        Xposs = Xposs + 25;
        l_connect = new JLabel("Disconnected", SwingConstants.LEFT );
        l_connect.setBounds(Xposs, Yposs, 200, 25);
        panel1.add(l_connect);

        Xposs = 20;
        Yposs = Yposs + 40;
        t_debug = new JTextArea();
        sCrollDebug = new JScrollPane(t_debug);
        sCrollDebug.setBounds(Xposs, Yposs, 600, 700);
        panel1.add(sCrollDebug);

        Xposs = 370;
        Yposs = Yposs + 705;    
        tf_DeStatus = new JTextField("");
        tf_DeStatus.setBounds(Xposs, Yposs, 20, 20);
        tf_DeStatus.setForeground(Color.white);
        tf_DeStatus.setBackground(Color.gray);
        tf_DeStatus.setEnabled(false);
        tf_DeStatus.setOpaque(false);
        LineBorder lineBorderDe = new LineBorder(Color.gray, 20, true);
        tf_DeStatus.setText("D");
        tf_DeStatus.setBorder(lineBorderDe);
        panel1.add(tf_DeStatus);

        Xposs += 30;
        bDebuglog = new JButton("Set Log");
        bDebuglog.setBounds(Xposs, (Yposs - 2), 100, 25);
        panel1.add(bDebuglog);
        bDebuglog.addActionListener(this); 

        Xposs += 110;
        bSaveDebug = new JButton("Save Log");
        bSaveDebug.setBounds(Xposs, (Yposs - 2), 110, 25);
        panel1.add(bSaveDebug);
        bSaveDebug.addActionListener(this); 

        //// --------------- OM LOG
        Xposs = 640;
        Yposs = 60;
        t_omlog = new JTextArea();
        sCrollOmlog = new JScrollPane(t_omlog);
        sCrollOmlog.setBounds(Xposs, Yposs, 630, 700);
        panel1.add(sCrollOmlog);

        Xposs += 350;
        Yposs += 705;
        tf_OMStatus = new JTextField("");
        tf_OMStatus.setBounds(Xposs, Yposs, 20, 20);
        tf_OMStatus.setForeground(Color.white);
        tf_OMStatus.setBackground(Color.gray);
        tf_OMStatus.setEnabled(false);
        tf_OMStatus.setOpaque(false);
        LineBorder lineBorderOM = new LineBorder(Color.gray, 20, true);
        tf_OMStatus.setText("D");
        tf_OMStatus.setBorder(lineBorderOM);
        panel1.add(tf_OMStatus);

        Xposs += 30;
        bOMlog = new JButton("Set Log");
        bOMlog.setBounds(Xposs, (Yposs - 2), 100, 25);
        panel1.add(bOMlog);
        bOMlog.addActionListener(this); 

        Xposs += 110;
        bSaveOM = new JButton("Save Log");
        bSaveOM.setBounds(Xposs, (Yposs - 2), 110, 25);
        panel1.add(bSaveOM);
        bSaveOM.addActionListener(this); 

        //Añadimos un nombre de la pestaña y el panel
        tb_panels.addTab("MCAS", panel1);
        
        //Realizamos lo mismo con el resto
        JPanel panel2=new JPanel();
        tb_panels.addTab("DIAMETER", panel2);
        //Componentes del panel2
        JLabel et_p2=new JLabel("Estas en el panel Diameter");
        panel2.add(et_p2);
        //--------------------------------------------------------
        JPanel panel3=new JPanel();
        //Componentes del panel3
        JLabel et_p3=new JLabel("Estas en el NGINSERVER(python)");
        panel3.add(et_p3);
        tb_panels.addTab("GLMPAS", panel3);
        //---------------------------------------------------------
        JPanel panel4=new JPanel();
        //Componentes del panel4
        JLabel et_p4=new JLabel("Estas en el panel ESAE (Build)");
        panel4.add(et_p4);
        tb_panels.addTab("ESAE_VM", panel4);
        getContentPane().add(tb_panels);
        
        // Read information from config.properties file
        ReadProperties properties = new ReadProperties();
        setTitle("Session : " + properties.getComputerName());
        tf_mcas_ip.setText(properties.get("MCAS_ip"));
        tf_mcas_user.setText(properties.get("MCAS_user"));
        tf_mcas_pass.setText(properties.get("MCAS_pass"));
        l_pc_id = properties.getComputerName();
        Check_MCAS_Status();
    };

    public void Check_MCAS_Status() {
        String Resultado_Session = "";
        RunCommandViaSsh Run_Command = new RunCommandViaSsh();
        LineBorder lineBorderGray = new LineBorder(Color.gray, 20, true);
        LineBorder lineBorderGreen = new LineBorder(Color.green, 20, true);
        LineBorder lineBorderRed = new LineBorder(Color.red, 20, true);
        tf_Status.setBorder(lineBorderGray);
        // Patsh to catch errors - /tmp/TEMP<pc_id> | cat /tmp/TEMP<pc_id>
        String TempPath = "/tmp/TEMP" + l_pc_id + " | cat /tmp/TEMP" + l_pc_id;
        Run_Command.SetHotsParameters(tf_mcas_ip.getText(), tf_mcas_user.getText(), tf_mcas_pass.getText().toString());
        try {
            Resultado_Session = Run_Command.runCommand("cd JMM 2> " + TempPath);
            if (!Resultado_Session.isEmpty()) {
                Resultado_Session = Run_Command.runCommand("mkdir JMM 2> " + TempPath);
            }
            Resultado_Session = Run_Command.runCommand("cd JMM/jmm" + l_pc_id + " 2> " + TempPath);
            if (!Resultado_Session.isEmpty()) {
                Resultado_Session = Run_Command.runCommand("mkdir JMM/jmm" + l_pc_id + " 2> " + TempPath);
                if (!Resultado_Session.isEmpty()) {
                    tf_Status.setBorder(lineBorderRed);
                    l_connect.setText("Error >>> Jmm Path not created !" + Resultado_Session + "!\n");
                } else {
                    tf_Status.setBorder(lineBorderGreen);
                    Resultado_Session = Run_Command.runCommand("hostname ");
                    l_connect.setText(Resultado_Session);
                }
            } else {
                tf_Status.setBorder(lineBorderGreen);
                Resultado_Session = Run_Command.runCommand("hostname ");
                l_connect.setText(Resultado_Session);
            }
        } catch (Exception errores) {
            l_connect.setText(errores.toString());
            l_connect.setText("Review IP-User-Pass");
        }
    }

    public Session InitDebuglog(Session pSession, JButton pButton, JTextField pTextField) throws Exception {
        JSch jsch = new JSch();
        pSession = jsch.getSession(tf_mcas_user.getText(), tf_mcas_ip.getText());
        String pwd = tf_mcas_pass.getText().toString();
        pSession.setPassword(pwd);
        Hashtable<String, String> config = new Hashtable<String, String>();
        config.put("StrictHostKeyChecking", "no");
        pSession.setConfig(config);
        pSession.connect(3000);
        pSession.setServerAliveInterval(3000);
        LineBorder lineBorderDe = new LineBorder(Color.green, 20, true);
        pTextField.setBorder(lineBorderDe);
        pButton.setText("Stop log");
        return pSession;
    }

    public void StartDebuglog(Session session, ChannelExec m_channelExec, JTextArea ptextArea  , String log_type) throws Exception {
        String cmd = "/cs/sn/bin/tailer " +  log_type;
        m_channelExec.setCommand(cmd);
        java.io.InputStream m_in = m_channelExec.getInputStream();
        m_channelExec.setPty(true);
        m_channelExec.connect();
        BufferedReader m_bufferedReader = new BufferedReader(new InputStreamReader(m_in));
        int ln_pos = 0;
        boolean active = true;
        while (active) {
            if (m_bufferedReader.ready()) {
                String line = m_bufferedReader.readLine();
                String tmp_line ="";
                while (line.length() >= 85){
                    ln_pos += 1;
                    tmp_line = ln_pos + " " + line.substring(0, 85) + "\n";
                    ptextArea.append(tmp_line);
                    line = line.substring(86, line.length());  
                }
                ln_pos += 1;
                tmp_line = ln_pos + " " + line + "\n";
                ptextArea.append(tmp_line);
                ptextArea.setCaretPosition(ptextArea.getDocument().getLength());
            }
        }
    }

    public void StopDebuglog(Session pSession, ChannelExec pChannel, Boolean Save, JButton pButton, JTextField pTextField) throws Exception {
        LineBorder lineBorderDe = new LineBorder(Color.gray, 20, true);
        pButton.setText("Set Log");
        pTextField.setBorder(lineBorderDe);
        pChannel.sendSignal("SIGINT");
        pChannel.disconnect();
        pSession.disconnect();
        if (Save) {
            // Guardar em Debug.log file ????
        } else {
            // Do nothing
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == bConectar) {
            Check_MCAS_Status();
        } else if (e.getSource() == bDebuglog) {
            if (bDebuglog.getText() == "Set Log") {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            t_debug.setText("");
                            SDebuglog = InitDebuglog(SDebuglog,bDebuglog,tf_DeStatus);
                            CDebuglog = (ChannelExec) SDebuglog.openChannel("exec");
                            StartDebuglog(SDebuglog, CDebuglog,t_debug,"de");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            } else if (bDebuglog.getText() == "Stop log"){
                try {
                    StopDebuglog(SDebuglog, CDebuglog,false,bDebuglog,tf_DeStatus);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        } else if (e.getSource() == bOMlog) {
            if (bOMlog.getText() == "Set Log") {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            t_omlog.setText("");
                            SOMlog= InitDebuglog(SOMlog,bOMlog,tf_OMStatus);
                            COMlog = (ChannelExec) SOMlog.openChannel("exec");
                            StartDebuglog(SOMlog, COMlog, t_omlog,"OM");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            } else if (bOMlog.getText() == "Stop log"){
                try {
                    StopDebuglog(SOMlog, COMlog,false,bOMlog,tf_OMStatus);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }  
    }   

    public static void main (String args[]) throws IOException, InterruptedException{
        JmmForm jmmForm1 = new JmmForm();
        jmmForm1.setResizable(false);
        jmmForm1.setExtendedState(JFrame.MAXIMIZED_BOTH);       
        jmmForm1.setVisible(true);
    }
}