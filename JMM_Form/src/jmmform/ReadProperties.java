package jmmform;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class ReadProperties {

    private Properties properties = new Properties();

    public ReadProperties() {
        try {
            InputStream inputStream = new FileInputStream("config.properties");
            properties.load(inputStream);
            inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getComputerName() {
        String hostname = "Unknown";
        try {
            InetAddress addr;
            addr = InetAddress.getLocalHost();
            hostname = addr.getHostName();
            System.out.println("HOST NAME " + hostname);
        } catch (UnknownHostException ex) {
            System.out.println("Hostname can not be resolved");
        }
        return hostname;
    }

    public String get(String key) {
        return this.properties.getProperty(key);
    }

    public String set(String key, String value) {
        String Result="";
        try {
            FileOutputStream outputStream = new FileOutputStream("config.properties");
            properties.setProperty(key, value);
            properties.store(outputStream, null);
            outputStream.close();
            Result = "ok";
        } catch (Exception e) {
            e.printStackTrace();
            Result = "Error";
        }
        return Result;
    }
}